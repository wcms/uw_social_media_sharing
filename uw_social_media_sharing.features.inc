<?php

/**
 * @file
 * uw_social_media_sharing.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_social_media_sharing_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "captcha" && $api == "captcha") {
    return array("version" => "1");
  }
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
