<?php
/**
 * @file
 * uw_social_media_sharing.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_social_media_sharing_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_social_media_sharing';
  $context->description = 'Display social media block on all pages';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~admin' => '~admin',
        '~admin/*' => '~admin/*',
        '~node/*/edit' => '~node/*/edit',
        '~node/add' => '~node/add',
        '~node/add/*' => '~node/add/*',
        '~user/*' => '~user/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_social_media_sharing-social_media_block' => array(
          'module' => 'uw_social_media_sharing',
          'delta' => 'social_media_block',
          'region' => 'content',
          'weight' => '49',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Display social media block on all pages');
  $export['uw_social_media_sharing'] = $context;

  return $export;
}
